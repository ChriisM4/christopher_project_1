package com.example;

import com.example.controller.ReimbursementController;
import com.example.controller.UserController;
import com.example.dao.ERSDBConnection;
import com.example.dao.ReimbursementDAO;
import com.example.dao.UserDAO;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.Javalin;

public class MainDriver {
	public static void main(String[] args) {
		
		UserController uCont = new UserController(new UserService(new UserDAO(new ERSDBConnection())));
		ReimbursementController rCont = new ReimbursementController(new ReimbursementService(new ReimbursementDAO(new ERSDBConnection())));
		
		Javalin app = Javalin.create(config->{
		config.addStaticFiles("/frontend");
		config.enableCorsForAllOrigins();
		});
		app.start(9012);
		app.post("/User/login", uCont.USERLOGIN);
		app.post("/Admin/login", uCont.ADMINLOGIN);
		app.get("/User/session", uCont.GETSESSUSER);
		app.post("/Reimb/decline", rCont.POSTALLDECLINEREIMB);
		app.post("/Reimb/accepted", rCont.POSTALLACCEPTEDREIMB);
		app.post("/Reimb/pending", rCont.POSTALLPENDINGREIMB);
		app.get("/Reimb/add", rCont.ADDREIMB);
		app.post("/User/completed", uCont.GETBYID);
	}
}
