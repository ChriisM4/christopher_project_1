package com.example.controller;

import com.example.model.Reimbursement;
import com.example.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {
	private ReimbursementService rServ;
	
	public ReimbursementController() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}
	
	public final Handler GETALLREIMB = (ctx) -> {
		rServ.getAllReimbursement();
		ctx.json(rServ.getAllReimbursement());
		ctx.status();
	};
	
	public final Handler POSTALLPENDINGREIMB = (ctx) ->{
		rServ.getAllPending();
		ctx.json(rServ.getAllPending());
		ctx.status();
	};
	
	public final Handler POSTALLACCEPTEDREIMB = (ctx) ->{
		rServ.getAllAccepted();
		ctx.json(rServ.getAllAccepted());
		ctx.status();
	};
	
	public final Handler POSTALLDECLINEREIMB = (ctx) ->{
		rServ.getAllDecline();
		ctx.json(rServ.getAllDecline());
		ctx.status();
	};
	
	public final Handler POSTAPPROVEREIMB = (ctx) ->{
		rServ.getAllAccepted();
		ctx.json(rServ.getAllAccepted());
		ctx.status();
	};
	
	public final Handler ADDREIMB = (ctx) ->{
		Reimbursement reimb = new Reimbursement();
		rServ.addReimbursement(reimb);
		ctx.json(rServ.addReimbursement(reimb));
		ctx.status(201);
	};
}
