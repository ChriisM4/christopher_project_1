package com.example.controller;

import com.example.dao.ERSDBConnection;
import com.example.dao.UserDAO;
import com.example.model.User;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	private UserService uServ;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}
	
	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public final Handler USERLOGIN = (ctx) ->{
		User user = uServ.verifyPassword(ctx.formParam("username"), ctx.formParam("password"));
		if(user != null) {
			ctx.sessionAttribute("currentuser");
			ctx.redirect("/html/user_home.html");
			System.out.println("Signed in");
		}else
			ctx.redirect("/html/error.html");
	};
	
	public final Handler ADMINLOGIN = (ctx) ->{
		User user = uServ.verifyPassword(ctx.formParam("username"), ctx.formParam("password"));
		if(user != null) {
			ctx.sessionAttribute("currentuser");
			ctx.redirect("/html/manager_home.html");
			System.out.println("Signed in");
		} else
			ctx.redirect("/html/error.html");
	};
	
	public final Handler GETSESSUSER = (ctx) ->{
		System.out.println((User)ctx.sessionAttribute("currentuser"));
		User user = (User)ctx.sessionAttribute("currentuser");
		ctx.json(user);
	};
	
	public final Handler GETALLUSER = (ctx) ->{
		UserDAO udao = new UserDAO(new ERSDBConnection());
		udao.queryAll();
		ctx.json(udao.queryAll());
		ctx.status();
	};
	
	public final Handler GETBYID = (ctx) ->{
		int id = Integer.parseInt(ctx.pathParam("id"));
		UserDAO uDao = new UserDAO(new ERSDBConnection());
		User user = uServ.findUserById(id);
		if(user.getUsername() ==  null) {
			ctx.status(404);
		}else {
			uDao.queryAll();
			ctx.json(uDao.queryAll());
			ctx.status();
		}
	};
}

