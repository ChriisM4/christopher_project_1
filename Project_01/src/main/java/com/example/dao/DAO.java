package com.example.dao;

import java.util.List;

public interface DAO<T> {
	
	T insert(T object) ;
	List<T> queryAll() ;
	List<T> queryById(int id) ;
	int getId();
}
