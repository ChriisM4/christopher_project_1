package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimbursement;

public class ReimbursementDAO implements DAO<Reimbursement>{
	
	private ERSDBConnection erscon;
	
	public ReimbursementDAO(ERSDBConnection ersdb) {
		super();
		this.erscon = ersdb;
	}

	@Override
	public Reimbursement insert(Reimbursement object) {
		try(Connection con = erscon.getDBConnection()){
			con.setAutoCommit(false);
			String sql = "INSERT INTO ers_reimbursement VALUES (?,?,?,0,?,?,81,3,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, getId());
			ps.setDouble(2, object.getAmount());
			ps.setString(3, object.getReimbSubmitted());
			ps.setString(4, object.getReimbDesc());
			ps.setInt(5, object.getReimbAuthor());
			ps.setInt(6, object.getReimbTypeId());
			ps.executeUpdate();
			con.commit();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return object;
	}

	@Override
	public List<Reimbursement> queryAll() {
		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT * FROM ers_reimbursement";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Reimbursement> list = new ArrayList<>();
			
			while(rs.next()) {
				list.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8),  rs.getInt(9)));
			}
			return list;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Reimbursement> queryById(int id) {
		List<Reimbursement> list = new ArrayList<>();
		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new Reimbursement (rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8),  rs.getInt(9)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int getId() {

		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT MAX(reimb_id) FROM ers_reimbursement";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) 
				return rs.getInt(1) +1;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/*public void approve(int id) {
		try(Connection con = erscon.getDBConnection()){
			String sql = "UPDATE ers_reimbursement SET reimb_status_id = 1, reimb_resolved = ?, reimb_resolver = ? WHERE reimb_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, .getReimbResolved());
			ps.executeQuery();
		}catch(SQLException r) {
			r.printStackTrace();
		}
	}*/
	
	public List<Reimbursement> AllPending(){
		try(Connection con = erscon.getDBConnection()){
			List<Reimbursement> listReimb = new ArrayList<>();
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = 2";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				listReimb.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8),  rs.getInt(9)));
			}
			return listReimb;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Reimbursement> AllAccepted(){
		try(Connection con = erscon.getDBConnection()){
			List<Reimbursement> listReimb = new ArrayList<>();
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = 1";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				listReimb.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8),  rs.getInt(9)));
			}
			return listReimb;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Reimbursement> AllDecline(){
		try(Connection con = erscon.getDBConnection()){
			List<Reimbursement> listReimb = new ArrayList<>();
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = 3";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				listReimb.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8),  rs.getInt(9)));
			}
			return listReimb;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Reimbursement> getByStatus(int status){
		List<Reimbursement> list = new ArrayList<>();
		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_status_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6), rs.getInt(7), rs.getInt(8),  rs.getInt(9)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

}
