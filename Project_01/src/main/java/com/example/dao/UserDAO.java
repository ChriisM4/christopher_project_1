package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.User;
import com.example.model.UserRole;


public class UserDAO implements DAO<User> {
	
	private ERSDBConnection erscon;
	
	public UserDAO(ERSDBConnection ersdb) {
		super();
		this.erscon = ersdb;
	}

	@Override
	public User insert(User object) {
		try(Connection con = erscon.getDBConnection()){
			String sql = "INSERT INTO ers_users VALUES (?,?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, getId());
			ps.setString(2, object.getUsername());
			ps.setString(3, object.getPassword());
			ps.setString(4, object.getFirstName());
			ps.setString(5, object.getLastName());
			ps.setString(6, object.getEmail());
			ps.setString(7, object.getRole().getRoleName());
			ps.executeUpdate();
			System.out.println("done");
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return object;
	}

	@Override
	public List<User> queryAll(){
		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT * FROM ers_users INNER JOIN ers_user_roles ON user_role_id = ers_user_role_id";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<User> userList = new ArrayList<>();
			
			while(rs.next()) {
				userList.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), new UserRole(rs.getInt(7), rs.getString(7))));
			}
			return userList;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<User> queryById(int id) {
		List<User> list = new ArrayList<>();
		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT * FROM ers_users INNER JOIN ers_user_roles ON user_role_id = ers_user_role_id WHERE ers_users_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				list.add(new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), new UserRole(rs.getInt(7), rs.getString(7))));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int getId() {
		try(Connection con = erscon.getDBConnection()){
			int id = 0;
			String sql = "SELECT MAX(ers_users_id) FROM ers_users";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				id = rs.getInt(1) +1;
			}
			return id;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public User getByCredentials(String firstname, String lastname) {
		User user = null;
		try(Connection con = erscon.getDBConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT * FROM ers_users WHERE user_first_name = ? AND user_last_name = ?");
			ps.setString(1, firstname);
			ps.setString(2, lastname);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), new UserRole(rs.getInt(7), rs.getString(7)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public User getByUsername(String username) {
		User user = null;
		try(Connection con = erscon.getDBConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT * FROM ers_users WHERE ers_username = ?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), new UserRole(rs.getInt(7), rs.getString(7)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	public boolean isAdmin(int id) {
		boolean isAdmin = false;
		switch(id) {
		case 1: case 4: case 5: isAdmin = true; break;
		case 2: case 3: case 6: case 7: case 8: case 9: case 10: case 11: isAdmin = false; break;
		default: break;
		}
		return isAdmin;
	}
	
	public UserRole UserIsAdmin(String username) {
		UserRole user = null;
		try(Connection con = erscon.getDBConnection()){
			String sql = "SELECT user_role_id FROM ers_users WHERE ers_username =?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				user = new UserRole(rs.getInt(1));
			}
			if(isAdmin(user.getRoleId())) {
				return user;
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
