package com.example.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.model.UserRole;

public class UserRoleDao implements DAO<UserRole>{
	
	private ERSDBConnection erscon;

	@Override
	public UserRole insert(UserRole object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserRole> queryAll() {
		List<UserRole> list = new ArrayList<>();
		try(Connection con = erscon.getDBConnection()){
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM ers_user_roles");
			
			while(rs.next()) {
				list.add(new UserRole(rs.getInt(1), rs.getString(2)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<UserRole> queryById(int id) {
		List<UserRole> uRole = new ArrayList<>();
		try(Connection con = erscon.getDBConnection()){
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM ers_user_roles WHERE ers_user_role_id = ?");
			
			while(rs.next()) {
				uRole.add(new UserRole(rs.getInt(1), rs.getString(2)));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return uRole;
	}

	@Override
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

}
