package com.example.model;

public class UserRole {
	private int roleId;
	private String roleName;
	
	public UserRole() {
		// TODO Auto-generated constructor stub
	}

	public UserRole(int roleId, String roleName) {
		super();
		this.roleId = roleId;
		switch(roleId) {
		case 1: case 4: case 5: this.roleName = "ADMIN"; break;
		case 2: case 3: case 6: case 7: case 8: case 9: case 10: case 11: this.roleName="USER"; break;
		default: break;
		}
	}
	
	public UserRole(int id) {
		this.roleId = id;
	}
	
	public UserRole(String name) {
		this.roleName = name;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		switch(roleId) {
		case 1: case 4: case 5: this.roleName = "ADMIN"; break;
		case 2: case 3: case 6: case 7: case 8: case 9: case 10: case 11: this.roleName="USER"; break;
		default: break;
		}
	}

	@Override
	public String toString() {
		return "UserRole [roleId=" + roleId + ", roleName=" + roleName + "]";
	}
}
