package com.example.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.example.dao.ReimbursementDAO;
import com.example.model.Reimbursement;

public class ReimbursementService {
	
	public ReimbursementDAO rDao;
	
	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementService(ReimbursementDAO rDao) {
		super();
		this.rDao = rDao;
	}


	public List<Reimbursement> getAllReimbursement(){
		List<Reimbursement> reimb = rDao.queryAll();
		return reimb;
	}
	
	public Reimbursement addReimbursement(Reimbursement reimb) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("YYYY/MM/DD");
		LocalDateTime now = LocalDateTime.now();
		String time = dtf.format(now);
		reimb.setReimbSubmitted(time);
		reimb.setReimbStatusId(3);
		return rDao.insert(reimb);
	}
	
	public List<Reimbursement> getReimbById(int id){
		List<Reimbursement> list = rDao.queryById(id);
		return list;
	}
	
	public List<Reimbursement> getAllPending(){
		return rDao.AllPending();
	}
	
	public List<Reimbursement> getAllAccepted(){
		return rDao.AllAccepted();
	}
	
	public List<Reimbursement> getAllDecline(){
		return rDao.AllDecline();
	}

}
