package com.example.service;

import java.util.List;

import com.example.dao.UserDAO;
import com.example.model.User;
import com.example.model.UserRole;

public class UserService {
	
	public UserDAO udao;
	public UserRole urole;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	public UserService(UserDAO udao) {
		super();
		this.udao = udao;
	}
	
	public UserService(UserRole urole) {
		super();
		this.urole = urole;
	}

	public List<User> findAllUser(){
		return udao.queryAll();
	}
	
	public User findUserById(int id) {
		return (User) udao.queryById(id);
	}
	
	public User findUserByUsername(String username) {
		return udao.getByUsername(username);
	}
	
	public User verifyPassword(String username, String pwd) {
		User user = findUserByUsername(username);
		if(user.getPassword().equals(pwd)) {
			return user;
		}
		return null;
	}
	
}
