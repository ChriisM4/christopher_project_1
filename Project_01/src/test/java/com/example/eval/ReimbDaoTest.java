package com.example.eval;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ERSDBConnection;
import com.example.dao.ReimbursementDAO;
import com.example.model.Reimbursement;

public class ReimbDaoTest {
	
	@Mock
	private ERSDBConnection edb;
	
	@Mock
	private Connection con;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	private Reimbursement reimb;
	
	
	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		when(edb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		reimb = new Reimbursement(213, 4352.4, "2016-03-01", "", "This is a description", 1, 1, 2, 3);
	}
	
	@Test
	public void testInsert() throws Exception{
		new ReimbursementDAO(edb).insert(reimb);
	}
	
	@Test
	public void testQueryAll() throws Exception{
		List<Reimbursement> list = new ReimbursementDAO(edb).queryAll();
		for(Reimbursement reimb:list) {
			System.out.println(reimb);
		}
	}
	
	@Test
	public void testQueryPending() throws Exception{
		System.out.println(new ReimbursementDAO(edb).AllPending());
	}
	
	@Test
	public void testQueryAccepted() throws Exception{
		System.out.println(new ReimbursementDAO(edb).AllAccepted());
	}
	
	@Test
	public void testQueryDecline() throws Exception{
		System.out.println(new ReimbursementDAO(edb).AllDecline());
	}
	
	@Test
	public void testQueryById() throws Exception{
		System.out.println(new ReimbursementDAO(edb).queryById(3));
	}
}
