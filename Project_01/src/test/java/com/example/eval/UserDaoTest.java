package com.example.eval;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ERSDBConnection;
import com.example.dao.UserDAO;
import com.example.model.User;
import com.example.model.UserRole;

public class UserDaoTest {
	
	@Mock
	private ERSDBConnection edb;
	
	@Mock
	private Connection con;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	private User user;
	private UserRole role;
	
	@BeforeEach
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		when(edb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		role = new UserRole(4, "Admin");
		user = new User(15, "chris1", "Avengers", "Christopher", "Mendy", "chris@avengers.org", role);
	}
	
	@Test
	public void testInsert() throws Exception{
		new UserDAO(edb).insert(user);
	}
	
	@Test
	public void testQueryAll() {
		List<User> users = new UserDAO(edb).queryAll();
		for(User user:users) {
			System.out.println(user);
		}
	}
	
	@Test
	public void testQueryById() {
		System.out.println(new UserDAO(edb).queryById(4));
	}
	
	@Test
	public void testByCredential() {
		System.out.println(new UserDAO(edb).getByCredentials("Christopher", "Mendy"));
	}
	
}
